import Vue from 'vue'
import Router from 'vue-router'
import ownerview from './views/vwuser'
import ownerlistview from './views/vwuserlist'
import newuser from './views/vwuser'
import localvw from './views/vwlocales'
import accesovw from './views/vwaccess'
import ticketsvw from './views/vwtickets'
import vwticketsearch from './views/vwsearchticket'
import vwticketgana from './views/vwticketgana'
import vwdashboard from './views/vwdashboard'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes:[
        {
            path: '/userlist/:utype',
            name: 'Lista de Usuarios',
            component: ownerlistview
        },
        {
            path: '/user/new',
            name: 'Nuevo Usuario',
            component: newuser
        },
        {
            path: '/user/locals',
            name: 'Locales',
            component: localvw
        },
        {
            path: '/user/gameacc',
            name: 'Accesos',
            component: accesovw
        },
        {
            path: '/user/tickets',
            name: 'Enumera Tickets',
            component: ticketsvw
        },
        {
            path: '/tickets/buscar',
            name: 'Buscar Tickets',
            component: vwticketsearch
        },
        {
            path: '/tickets/esgana',
            name: 'Buscar Ticket Ganador',
            component: vwticketgana
        },
        {
            path: '/dashboard/showit',
            name: 'Dashboard',
            component: vwdashboard
        }
    ]
})